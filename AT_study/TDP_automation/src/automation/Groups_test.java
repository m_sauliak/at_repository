package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;


/**
 * Created by Mikhail_Sauliak on 5/6/2017.
 */

public class Groups_test {

    private WebDriver driver = DriverInitialization.INSTANCE.getDriver();

    Authorization logon = new Authorization(driver);



    private By _newGroup = By.xpath("//*[@id=\"ctl00_phMainContent_btnNewGroup_CD\"]");
    private  By _practiceGroup_txtBox = By.id("ctl00_phMainContent_cpGroupActions_txtName_I");

    private By _managerFirstName_txtBox = By.id("ctl00_phMainContent_cpGroupActions_txtAccountManagerFirstName_I");
    private  By _managerLastName_txtBox = By.id("ctl00_phMainContent_cpGroupActions_txtAccountManagerLastName_I");
    private  By _salesRepFirstName_txtBox = By.id("ctl00_phMainContent_cpGroupActions_txtSalesRepFirstName_I");
    private  By _salesRepLastName_txtBox = By.id("ctl00_phMainContent_cpGroupActions_txtSalesRepLastName_I");
    private  By _saveBtn = By.id("ctl00_phMainContent_cpGroupActions_pButtons_ctl03_CD");
    private  By _cancelBtn = By.id("ctl00_phMainContent_cpGroupActions_pButtons_ctl03_CD");
    private By _save_message = By.id("message-01");

    @BeforeSuite
    public void Start () throws Exception {
        logon.StartUp();
        logon.Auth("December2006","UAT_SPG_SUPERVISOR_0@OCM-PHX.DOM" );
        driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);


    }
    @BeforeMethod
    public void Group_Page(){
        logon.getGroupsUrl();
    }

    @Test(dataProvider = "NewGroupData")

    public  void Group_test(String sPractice_group, String sAM_First_name, String sAM_Last_name, String sSR_First_name, String sSR_Last_name, String act_Status, String man_Status)
            throws  Exception{


        driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);

        driver.findElement(_newGroup).click();
        driver.findElement(_practiceGroup_txtBox).sendKeys(sPractice_group);
        driver.findElement(_managerFirstName_txtBox).sendKeys(sAM_First_name);
        driver.findElement(_managerLastName_txtBox).sendKeys(sAM_Last_name);
        driver.findElement(_salesRepFirstName_txtBox).sendKeys(sSR_First_name);
        driver.findElement(_salesRepLastName_txtBox).sendKeys(sSR_Last_name);

        //Activity dropdown
        driver.findElement(By.id("ctl00_phMainContent_cpGroupActions_cbActivityStatus_I")).click();
        if (act_Status.equals("INACTIVE")){


            driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpGroupActions_cbActivityStatus_DDD_L_LBI1T0\"]")).click();
        }
        else {

            driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpGroupActions_cbActivityStatus_DDD_L_LBI0T0\"]")).click();
        }

       // Management Status dropdown
        driver.findElement(By.id("ctl00_phMainContent_cpGroupActions_cbManagementStatus")).click();
        if (man_Status.equals("UNMANAGED")){

            driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpGroupActions_cbManagementStatus_DDD_L_LBI1T0\"]")).click();
        }
        else {
            driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpGroupActions_cbManagementStatus_DDD_L_LBI0T0\"]")).click();
        }


       // driver.findElement(_cancelBtn).click();
        driver.findElement(_saveBtn).click();



    }

    @AfterMethod
    public void New_Group (){
        if (_save_message.equals("Group has been saved.")) {

            System.out.println("New group is created");
        }
    }
    @DataProvider
    public Object[][] NewGroupData() throws Exception {

        Object[][] testObjArray = ExcellDataGroups.getTableArray("D://soft//tests//loginData.xls", "Group_pairs");
        return (testObjArray);
    }

    @AfterSuite
    public void Quit() {
        if (driver != null) driver.quit();
    }
}
