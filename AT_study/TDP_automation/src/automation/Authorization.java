package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;

import java.util.concurrent.TimeUnit;

/**
 * Created by Mikhail_Sauliak on 5/6/2017.
 */
public class Authorization  {

    private WebDriver driver = DriverInitialization.INSTANCE.getDriver();
    public static String baseUrl = "https://physicians.onecallcm.com/EnterpriseWebFrameworkPortal";
    public static String groupsUrl ="https://physicians.onecallcm.com/EnterpriseWebFrameworkPortal?groups";
    private static By _login = By.id("cpLogin_txtUserName_I");
    private static By _password = By.id("cpLogin_txtPassword_I");
    private static By _submit = By.id("cpLogin_btnLogin_CD");
    public static By _findAnchor = By.id ("ctl00_phMainContent_pButtons_btnFind_CD");
    public static By _newGroup = By.xpath("//*[@id=\"ctl00_phMainContent_btnNewGroup_CD\"]");
    public Authorization(WebDriver driver) {
        this.driver = driver;
    }




    public void StartUp () throws  Exception {

        //Gecko driver to launch selenium
        driver.get(baseUrl);
        driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);

        System.out.println("Auth page is displayed");

    }

    public void Auth (String sPassword, String sUserName){
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

        driver.findElement(_login).sendKeys(sUserName);

        driver.findElement(_password).sendKeys(sPassword);

        driver.findElement(_submit).click();
        driver.manage().timeouts().pageLoadTimeout(50,TimeUnit.SECONDS);

        driver.findElement(_findAnchor).isDisplayed();

        System.out.println("Login is successfull");
    }

    public void getGroupsUrl(){
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.navigate().to("https://physicians.onecallcm.com/EnterpriseWebFrameworkPortal/Modules/ERM/TreatingDoctor/Default.aspx?group");

        System.out.println("Groups page is displayed");
    }
    public void getProvidersUrl(){
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.navigate().to("https://physicians.onecallcm.com/EnterpriseWebFrameworkPortal/Modules/ERM/TreatingDoctor/Default.aspx?provider");

        System.out.println("Providers page is displayed");
    }


    @DataProvider
    public Object[][] CorrectAuth() throws Exception{

        Object[][] testObjArray = DataProviderWithExcel.getTableArray("D://soft//tests//loginData.xls","Sheet1");
        return (testObjArray);
    }
}

