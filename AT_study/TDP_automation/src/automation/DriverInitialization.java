package automation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Mikhail_Sauliak on 5/6/2017.
 */

public enum DriverInitialization {
    INSTANCE;
    private WebDriver driver;



    DriverInitialization(){

        if (driver == null) {
            try {
                System.setProperty("webdriver.gecko.driver", "d:\\soft\\geckodriver.exe");
                // Walkaround certificate trouble
                ProfilesIni allProfiles = new ProfilesIni();
                FirefoxProfile myProfile = allProfiles.getProfile("default");
                myProfile.setAcceptUntrustedCertificates(true);
                myProfile.setAssumeUntrustedCertificateIssuer(false);
            driver = new FirefoxDriver(myProfile);
            } catch (Exception e) {
                throw new ExceptionInInitializerError(e);
            }
        }
    }

    public WebDriver getDriver(){
        driver.manage().window().maximize();

        return driver;
         }
    }


