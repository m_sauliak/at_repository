package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mikhail_Sauliak on 5/10/2017.
 */
public class Providers_test {

    private WebDriver driver = DriverInitialization.INSTANCE.getDriver();

    private By _newProvider = By.xpath(".//*[@id='ctl00_phMainContent_btnNewProvider_CD']/span");
    private By _provName = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_txtProviderName_I']");
   // private By _serviceType = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbServiceType_I']");
    private By _address = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_txtAddress_I']");
    private By _city = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_txtCity_I']");
    private By _phoneNumber = By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_txtPhoneNumber\"]");
  //   private By _actStatus = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbActivityStatus_I']");
    // private By _manStatus = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbManagementStatus_I']");
    private By _state = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbState_I']");
    private By _zip = By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_txtZip\"]");
    private By _fax = By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_txtFaxNumber\"]");
    private By _sysCode = By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_txtSysCode\"]");
    private By _note = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_txtNote_I']");
    private By _addPracticeBtn = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_btnAddPractice_CD']/span");
    private By _saveBtn = By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_pButtons_ctl03_B\"]");
    private By _cancelBtn = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_pButtons_ctl04_CD']/span");

    //Auth object
    Authorization logon = new Authorization(driver);
    //a[@id='ctl00_phMainContent_cpProviderActions_cpSearchPractice_ucSearchPractice_pcSearchPractice_pContent_cpResults_gvResults_tccell0_0']/@href
    //Practice search window
    private By _practiceSearchState = By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_cpSearchPractice_ucSearchPractice_pcSearchPractice_pContent_cbState\"]/tbody/tr/td[2]");
    private By _searchBtn = By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_cpSearchPractice_ucSearchPractice_pcSearchPractice_pButtons_btnSearch_CD\"]");
    private By _gridFirstResultLink = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cpSearchPractice_ucSearchPractice_pcSearchPractice_pContent_cpResults_gvResults_tccell0_0']/a");
    private By _gridSecondResultLink = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cpSearchPractice_ucSearchPractice_pcSearchPractice_pContent_cpResults_gvResults_tccell1_0']/a");
    private By _gridThirdResultLink = By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cpSearchPractice_ucSearchPractice_pcSearchPractice_pContent_cpResults_gvResults_tccell2_0']/a");
    private By _practiceSearchForm = By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_cpSearchPractice_ucSearchPractice_pcSearchPractice_PW-1\"]");

    @BeforeSuite
    public void Start () throws Exception {
        logon.StartUp();
        logon.Auth("December2006", "UAT_SPG_SUPERVISOR_0@OCM-PHX.DOM");
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
    }

    @BeforeMethod
    public void Group_Page(){
        logon.getProvidersUrl();
    }

    @Test (dataProvider = "NewProviderData", priority = 2)
    public void Provider_test (String sProvName, String sCity, String sAddress, String sActiveStatus, String sManStatus, String sServiceType, String sSysCode, String sPhone,
                               String sFax, String sState, String sZip, String sNote)  throws  Exception{

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.findElement(_newProvider).click();
        //Text Fields

        driver.findElement(_provName).sendKeys(sProvName);

        driver.findElement(_city).sendKeys(sCity);

        driver.findElement(_address).sendKeys(sAddress);

        //activity dropdown
        driver.findElement(By.id("ctl00_phMainContent_cpProviderActions_cbActivityStatus_I")).click();
        Thread.sleep(100);
        if (sActiveStatus.equals("INACTIVE")){
            //*[@id="ctl00_phMainContent_cpProviderActions_cbActivityStatus_DDD_L_LBI1T0"] - inactive option in dropdown table

            driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_cbActivityStatus_DDD_L_LBT\"]")).sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
        }
        Thread.sleep(100);
      /*
        else {

            driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_cbActivityStatus_DDD_L_LBI0T0\"]")).click();
        }
*/
        // Management Status dropdown
        driver.findElement(By.id("ctl00_phMainContent_cpProviderActions_cbManagementStatus")).click();
        Thread.sleep(100);
        if (sManStatus.equals("UNMANAGED")){

            //*[@id="ctl00_phMainContent_cpProviderActions_cbManagementStatus_DDD_L_LBI1T0"] - unmanaged option in dropdown table
            driver.findElement(By.id("ctl00_phMainContent_cpProviderActions_cbManagementStatus")).sendKeys(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER);
        }
        Thread.sleep(100);
        /*
        else {
            driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_cbManagementStatus_DDD_L_LBI0T0\"]")).click();
        }
        */
        //Service Type dropdown   //*[@id="ctl00_phMainContent_cpProviderActions_cbServiceType_I"]
        driver.findElement(By.id("ctl00_phMainContent_cpProviderActions_cbServiceType_I")).click();
        Thread.sleep(500);
        System.out.println("Service type dropdown is opened");

        if (sServiceType.equals("DIAGNOSTICS")){
            driver.findElement(By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbServiceType_DDD_L_LBI0T0']")).click();
                 //   sendKeys(Keys.ARROW_DOWN, Keys.ENTER, Keys.TAB);
        }
        if (sServiceType.equals("RADIOLOGY")){
            driver.findElement(By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbServiceType_DDD_L_LBI1T0']")).click();
        }
        if (sServiceType.equals("NEUROLOGY/EMG")){
            driver.findElement(By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbServiceType_DDD_L_LBI2T0']")).click();
                   // sendKeys(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER, Keys.TAB);
        }
        if (sServiceType.equals("PHYSICAL THERAPY")){
            driver.findElement(By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbServiceType_DDD_L_LBI3T0']")).click();
                  //  sendKeys(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER, Keys.TAB);
        }
        if (sServiceType.equals("EDM/HH")){
            driver.findElement(By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbServiceType_DDD_L_LBI4T0']")).click();
                 //   sendKeys(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER, Keys.TAB);
        }
        if (sServiceType.equals("DENTAL")){
            driver.findElement(By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbServiceType_DDD_L_LBI5T0']")).click();
                   // sendKeys(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER, Keys.TAB);
        }
        if (sServiceType.equals("DIAGNOSTIC")){
            driver.findElement(By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbServiceType_DDD_L_LBI6T0']")).click();
                   // sendKeys(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER, Keys.TAB);
        }

        Thread.sleep(500);
        WebElement serviceInput = driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_cbServiceType_I\"]"));
        serviceInput.getText();
        if (serviceInput == null){
            System.out.println("Some troubles appear. Service type was not chosen");
        }

        driver.findElement(_sysCode).click();
        driver.findElement(_sysCode).sendKeys(sSysCode);

        driver.findElement(_phoneNumber).click();
        driver.findElement(_phoneNumber).sendKeys(Keys.HOME,sPhone);

        driver.findElement(_fax).click();
        driver.findElement(_fax).sendKeys(Keys.HOME, sFax);

        driver.findElement(_state).sendKeys(sState);

        driver.findElement(_zip).click();
        driver.findElement(_zip).sendKeys(Keys.HOME,sZip);

        driver.findElement(_note).sendKeys(sNote);




        driver.findElement(_saveBtn).click();
        Thread.sleep(500);
        WebElement confirmation = driver.findElement(By.xpath("//*[@id=\"message-01\"]"));


           System.out.println(confirmation.getText());



    }
    @Test (dataProvider = "NewProviderDataGrid", priority = 3)
    public void Provider_with_grid (String sfields, String snoteData, String slinks) throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.findElement(_newProvider).click();

        if (sfields.equals("fullfilled all fields")) {
            allFields();

        } else {
            requiredFields();

        }
        if (snoteData.equals("with Note")){
            noteData();
        }
        if (slinks.equals("with 1 link")){
            addPractice();
        }
        else if (slinks.equals("with 5 links")) {
            for (int i=0; i<=4; i++ ){
                addPractice();
            }
        }
        driver.findElement(_saveBtn).click();
        Thread.sleep(500);
        WebElement confirmation = driver.findElement(By.xpath("//*[@id=\"message-01\"]"));


        System.out.println(confirmation.getText());

    }
    public void allFields () throws InterruptedException {
        driver.findElement(_provName).sendKeys("Provider Name");

        driver.findElement(_city).sendKeys("City");

        driver.findElement(_address).sendKeys("Address");
        driver.findElement(_sysCode).click();
        driver.findElement(_sysCode).sendKeys("111111");

        driver.findElement(_phoneNumber).click();
        driver.findElement(_phoneNumber).sendKeys(Keys.HOME,"12345678900");

        driver.findElement(_fax).click();
        driver.findElement(_fax).sendKeys(Keys.HOME, "12345678900");

        driver.findElement(_state).sendKeys("AK");

        driver.findElement(_zip).click();
        driver.findElement(_zip).sendKeys(Keys.HOME,"12345");

        //driver.findElement(By.id("ctl00_phMainContent_cpProviderActions_cbServiceType_I")).click();
        driver.findElement(By.id("ctl00_phMainContent_cpProviderActions_cbServiceType_I")).sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
        //System.out.println("Service type dropdown is opened");
     //   Thread.sleep(500);

       //driver.findElement(By.xpath(".//*[@id='ctl00_phMainContent_cpProviderActions_cbServiceType_DDD_L_LBI0T0']")).click();


    }
    public void requiredFields() throws InterruptedException {
        driver.findElement(_provName).sendKeys("Provider Name");
        driver.findElement(By.id("ctl00_phMainContent_cpProviderActions_cbServiceType_I")).sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
        Thread.sleep(500);
        System.out.println("Service type is chosen");

    }
    public void noteData(){
        driver.findElement(_note).sendKeys("Note information data");
    }
    public void addPractice() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);


        driver.findElement(_addPracticeBtn).click();
        System.out.println("Practice search window is displayed");
        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_cpSearchPractice\"]")).sendKeys(Keys.TAB,Keys.TAB,Keys.ARROW_DOWN);

        driver.findElement(_searchBtn).click();
        System.out.println("Practices are searched");
        Thread.sleep(500);

        driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_cpSearchPractice_ucSearchPractice_pcSearchPractice_pContent_txtFaxNumber\"]")).click();
        Thread.sleep(500);
        driver.findElement(By.xpath("//*[@id=\"ctl00_phMainContent_cpProviderActions_cpSearchPractice_ucSearchPractice_pcSearchPractice_pContent_txtFaxNumber\"]")).sendKeys(Keys.TAB,Keys.TAB,Keys.ENTER);
        System.out.println("Practice is added");

        Thread.sleep(500);


    }


    @DataProvider
    public Object[][] NewProviderData() throws Exception {

        Object[][] testObjArray = ExcellDataProviders.getTableArray("D://soft//tests//loginData.xls", "Provider_pairs");
        return (testObjArray);
    }

    @DataProvider
    public Object[][] NewProviderDataGrid() throws Exception {

        Object[][] testObjArray = ExcellDataProvidersGrid.getTableArray("D://soft//tests//loginData.xls", "Provider_grid");
        return (testObjArray);
    }


    @AfterSuite
    public void Quit() {
        if (driver != null) driver.quit();
    }
}
