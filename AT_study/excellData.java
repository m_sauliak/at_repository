package automation;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;



public class excellData {
	 
	public static HSSFSheet ExcelWSheet;

	public static HSSFWorkbook ExcelWBook;

	public static HSSFCell Cell;

	
public static Object[][] getTableArray(String FilePath, String SheetName) throws Exception {   

   String[][] tabArray = null;

   try {

	   FileInputStream ExcelFile = new FileInputStream(FilePath);

	   // Access test data sheet

	   ExcelWBook = new HSSFWorkbook(ExcelFile);

	   ExcelWSheet = ExcelWBook.getSheet(SheetName);

	   int startRow = 1;

	   int startCol = 1;

	   int ci,cj;

	   int totalRows = ExcelWSheet.getLastRowNum();

	   // Column count

	   int totalCols = 2;

	   tabArray=new String[totalRows][totalCols];

	   ci=0;

	   for (int i=startRow;i<=totalRows;i++, ci++) {           	   

		  cj=0;

		   for (int j=startCol;j<=totalCols;j++, cj++){

			   tabArray[ci][cj]=getCellData(i,j);

			   System.out.println(tabArray[ci][cj]);  

				}

			}

		}

	catch (FileNotFoundException e){

		System.out.println("Could not read the Excel sheet");

		e.printStackTrace();

		}

	catch (IOException e){

		System.out.println("Could not read the Excel sheet");

		e.printStackTrace();

		}

	return(tabArray);

	}
public static String getTestCaseName(String sTestCase)throws Exception{
	 
	String value = sTestCase;

	try{

		int position = value.indexOf("@");

		value = value.substring(0, position);

		position = value.lastIndexOf(".");	

		value = value.substring(position + 1);

		return value;

			}catch (Exception e){

		throw (e);

				}

	}

public static String getCellData(int RowNum, int ColNum) throws Exception{
	 
	   try{

		  Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);

		  String CellData = Cell.getStringCellValue();

		  return CellData;

		  }catch (Exception e){

			return"";

			}

		}

public static int getRowContains(String sTestCaseName, int colNum) throws Exception{
	 
	int i;

	try {

		int rowCount = excellData.getRowUsed();

		for ( i=0 ; i<rowCount; i++){

			if  (excellData.getCellData(i,colNum).equalsIgnoreCase(sTestCaseName)){

				break;

			}

		}

		return i;

			}catch (Exception e){

		throw(e);

		}

	}
public static int getRowUsed() throws Exception {
	 
	try{

		int RowCount = ExcelWSheet.getLastRowNum();

		return RowCount;

	}catch (Exception e){

		System.out.println(e.getMessage());

		throw (e);

	}

}

	

}
